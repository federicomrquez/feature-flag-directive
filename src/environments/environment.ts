// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBadEwQGZyxZ6I0sH46b9cKe92pF7inbdU',
    authDomain: 'test-27cdd.firebaseapp.com',
    projectId: 'test-27cdd',
    storageBucket: 'test-27cdd.appspot.com',
    messagingSenderId: '81030328645',
    appId: '1:81030328645:web:0a4dc83f13119c5abe0e2c',
    measurementId: 'G-49QTB4B6FK',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
