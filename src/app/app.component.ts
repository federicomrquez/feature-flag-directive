import { Component } from '@angular/core';
import { environment } from 'src/environments/environment';
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { FirebaseRemoteConfig } from './models/firebase-remote-config/firebase-remote-config';
import { RemoteConfigService } from './services/remote-config/remote-config.service';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  firebaseConfig = environment.firebase;
  constructor(private remoteConfigService: RemoteConfigService) {}

  ngOnInit() {
    const app = initializeApp(this.firebaseConfig);
    const analytics = getAnalytics(app);
    this.remoteConfigService.initialize(new FirebaseRemoteConfig(app));
  }
}
