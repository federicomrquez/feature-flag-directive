import { Component } from '@angular/core';
import { RemoteConfigService } from '../services/remote-config/remote-config.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  isShowing: boolean = false;

  constructor(private remoteConfigService: RemoteConfigService) {}

  toggleText() {
    this.isShowing = !this.isShowing;
  }

  showFeatureFlagValue() {
    const featureFlagValue = this.remoteConfigService.getFeatureFlag('ff_tab1Page_testText');
    console.log(featureFlagValue);
  }
}
