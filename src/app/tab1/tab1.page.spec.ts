import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';
import { FakeFeatureFlagDirective } from '../testing/fakes/fake-feature-flag.spec';

import { Tab1Page } from './tab1.page';

describe('Tab1Page', () => {
  let component: Tab1Page;
  let fixture: ComponentFixture<Tab1Page>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [Tab1Page, FakeFeatureFlagDirective],
      imports: [IonicModule.forRoot(), ExploreContainerComponentModule]
    }).compileComponents();

    fixture = TestBed.createComponent(Tab1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render text1', () => {
    const textEl = fixture.debugElement.query(By.css("ion-text.text1")).nativeElement;
    expect(textEl).toBeTruthy();
  });
  
  it('should render text2', () => {
    const textEl = fixture.debugElement.query(By.css("ion-text.text2")).nativeElement;
    expect(textEl).toBeTruthy();
  });

  it('should show text3 when button clicked', () => {
    fixture.debugElement.query(By.css("ion-button.button")).nativeElement.click();
    fixture.detectChanges();
    const textEl = fixture.debugElement.query(By.css("ion-text.text3"));
    expect(textEl).toBeTruthy();
  });

  it('should hide text3 when button clicked twice', () => {
    fixture.debugElement.query(By.css("ion-button.button")).nativeElement.click();
    fixture.debugElement.query(By.css("ion-button.button")).nativeElement.click();
    fixture.detectChanges();
    const textEl = fixture.debugElement.query(By.css("ion-text.text3"));
    expect(textEl).toBeFalsy();
  });
});
