import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.xcapit.featureFlagTest',
  appName: 'feature-flag-directive',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
